using VideoPresentation.Models;
using System.Collections.Generic;
using VideoPresentation.Entities;
using VideoPresentation.Infrastructure.Database.VideoTags;

namespace VideoPresentation.Infrastructure.Database
{
    public interface IVideosRepository
    {
        List<Video> GetAllVideos();
        void RemoveVideo(Video deletedVideo);
        List<Video> SearchVideo(string fileName);
        Video GetVideoById(int id);
        void EditVideo(Video editedVideo);
        void SaveVideo(Video video, byte[] fileBytes, string fileName, string json);
        List<TagWeight> GetTagWeightsForVideo(int videoID);

        List<TagWeight> FindTag(int tagID);

        List<Video> GetVideosWithTag(int tagID);

        List<TagPosition> GetJsonTagById(int id);
    }
}