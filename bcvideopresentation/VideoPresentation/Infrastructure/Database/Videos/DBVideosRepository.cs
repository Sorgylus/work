using System.Collections.Generic;
using System.IO;
using System.Linq;
using VideoPresentation.Connection;
using VideoPresentation.Entities;
using VideoPresentation.Entities.JSON;
using VideoPresentation.Infrastructure.Database.VideoTags;
using VideoPresentation.Models;
using Microsoft.EntityFrameworkCore;

namespace VideoPresentation.Infrastructure.Database
{
    public class DBVideosRepository : IVideosRepository
    {
        public DBVideosRepository()
        {
        }

        public void RemoveVideo(Video deletedVideo)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                var deleteVideo = dbContext.videos.Where(x => x.ID == deletedVideo.ID).FirstOrDefault();
                dbContext.videos.Remove(deleteVideo);
                dbContext.SaveChanges();
            }
        }

        public List<Video> GetAllVideos()
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                return dbContext.videos.ToList();
            }
        }

        public void SaveVideo(Video video, byte[] fileBytes, string fileName, string json)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                video.VideoName = fileName;
                dbContext.videos.Add(video);

                dbContext.SaveChanges();
            }
                  

            using (VideoDbContext dbContext = new VideoDbContext())
            {
                List<Tag> allTags = dbContext.tags.ToList();

                TagsJson.Rootobject parsedJson = Newtonsoft.Json.JsonConvert.DeserializeObject<TagsJson.Rootobject>(json);

                foreach (TagsJson.TagsInfoData tid in parsedJson.GetTagListsList())
                {
                    if (tid.TagInfos == null)
                        continue;
                    float predCount = 0;
                    int sections = 0;

                    Dictionary<float, float> tagdic = new Dictionary<float, float>();


                    foreach (TagsJson.TagInfo tagI in tid.TagInfos)
                    {
                        if (tagdic.ContainsKey(tagI.time))
                        {
                            if (tagI.prediction > tagdic[tagI.time])
                            {

                                tagdic[tagI.time] = tagI.prediction;
                            }
                        }
                        else
                        {
                            tagdic.Add(tagI.time, tagI.prediction);
                        }
                    }

                    foreach (KeyValuePair<float, float> dic in tagdic)
                    {
                        TagPosition tags = new TagPosition();
                        tags.Prediction = dic.Value;
                        tags.Time = dic.Key;
                        tags.VideoID = video.ID;
                        tags.Tag = allTags.Where(x => x.Name == tid.TagName).First();
                        dbContext.tagPositions.Add(tags);
                        predCount += dic.Value;
                        sections++;
                    }


                    TagWeight tagWeight = new TagWeight();
                    if ((int)((predCount * 100) / sections) != 0)
                    {
                        tagWeight.Percentage = (int)((predCount * 100) / sections);
                    }
                    tagWeight.Tag = allTags.Where(x => x.Name == tid.TagName).First();
                    tagWeight.VideoID = video.ID;
                    dbContext.TagWeight.Add(tagWeight);
                }
                dbContext.SaveChanges();
            }


        }
        public List<Video> SearchVideo(string fileName)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                return dbContext.videos.Where(x => x.VideoTitle.Contains(fileName)).ToList();
            }
        }
        public List<TagWeight> FindTag(int tagID)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                return dbContext.TagWeight.Where(x => x.Tag.ID == tagID).ToList();
            }
        }

        public Video GetVideoById(int id)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                return dbContext.videos.Where(x => x.ID == id).FirstOrDefault();
            }

        }

        public List<Video> GetVideosWithTag(int tagID)
        {
            using (VideoDbContext vdbc = new VideoDbContext())
            {
                List<int> videoIDS = vdbc.TagWeight.Where(x => x.Tag.ID == tagID && x.Percentage !=0).Select(t => t.VideoID).ToList();
                return vdbc.videos.Where(x => videoIDS.Contains(x.ID)).ToList();
            }
        }

        public List<TagWeight> GetTagWeightsForVideo(int videoID)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                return dbContext.TagWeight.Include(x => x.Tag).Where(x => x.VideoID == videoID).ToList();
            }
        }
        public List<TagPosition> GetJsonTagById(int id)
        {
            using(VideoDbContext dbContext = new VideoDbContext())
            { 
                return dbContext.tagPositions.Include(x => x.Tag).Where(x => x.VideoID == id).ToList();
            }
        }


        public void EditVideo(Video editedVideo)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                Video video = dbContext.videos.Where(x => x.ID == editedVideo.ID).FirstOrDefault();
                video.VideoTitle = editedVideo.VideoTitle;
                video.VideoCommentCs = editedVideo.VideoCommentCs;
                video.VideoCommentEn = editedVideo.VideoCommentEn;
                video.AuthorCommentCs = editedVideo.AuthorCommentCs;
                video.AuthorCommentEn = editedVideo.AuthorCommentEn;
                video.Year = editedVideo.Year;
                video.AdditionalYear = editedVideo.AdditionalYear;
                video.Series = editedVideo.Series;
                video.PrequelCs = editedVideo.PrequelCs;
                video.PrequelEn = editedVideo.PrequelEn;
                video.CitationCs = editedVideo.CitationCs;
                video.CitationEn = editedVideo.CitationEn;
                video.ArtisticMedium = editedVideo.ArtisticMedium;
                video.TechnicalMedium = editedVideo.TechnicalMedium;
                video.PerformerCs = editedVideo.PerformerCs;
                video.PerformerEn = editedVideo.PerformerEn;
                video.VoicePerformerCs = editedVideo.VoicePerformerCs;
                video.VoicePerformerEn = editedVideo.VoicePerformerEn;
                video.ToolName = editedVideo.ToolName;
                video.ToolAuthor = editedVideo.ToolAuthor;
                video.ToolInfo = editedVideo.ToolInfo;
                video.AnnotationCs = editedVideo.AnnotationCs;
                video.AnnotationEn = editedVideo.AnnotationEn;
                video.AnnotationReferenceCs = editedVideo.AnnotationReferenceCs;
                video.AnnotationReferenceEn = editedVideo.AnnotationReferenceEn;
                video.SourcesCs = editedVideo.SourcesCs;
                video.SourcesEn = editedVideo.SourcesEn;
                video.MuniSource = editedVideo.MuniSource;
                video.VideoLength = editedVideo.VideoLength;
                video.AdditionalVideoLength = editedVideo.AdditionalVideoLength;
                video.AuthId = editedVideo.AuthId;
                video.CoAuthId = editedVideo.CoAuthId;

                dbContext.videos.Update(video);
                dbContext.SaveChanges();
            }
        }


    }
}


