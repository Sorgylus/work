﻿using VideoPresentation.Models;
using System.Collections.Generic;
using System.Security.Claims;
using VideoPresentation.Entities;

namespace VideoPresentation.Infrastructure.Database
{
    public interface IUsersRepository
    {
        void SaveUser(User user);
        List<User> GetAllUsers();
        bool UserCheck(string userName, string password);
        void DeleteUser(int id);
        void EditExistingUser(User editedUser);
        User GetUserById(int id);
    }

}
