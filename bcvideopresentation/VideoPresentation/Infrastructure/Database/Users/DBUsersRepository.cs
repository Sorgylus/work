﻿using Microsoft.AspNetCore.Authentication.Cookies;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using VideoPresentation.Connection;
using VideoPresentation.Entities;
using VideoPresentation.Models;

namespace VideoPresentation.Infrastructure.Database
{
    public class DBUsersRepository : IUsersRepository
    {
        public DBUsersRepository()
        {
        }

        public void SaveUser(User user)
        {

            using (VideoDbContext dbContext = new VideoDbContext())
            {
                user.Password = HashSha256(user.Password);
                dbContext.users.Add(user);
                dbContext.SaveChanges();
            }

        }

        public List<User> GetAllUsers()
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                return dbContext.users.ToList();
            }
        }

        public bool UserCheck(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
            {
                return false;
            }
            using (VideoDbContext dbContext = new VideoDbContext())
            {
              
                if ((userName == "Admin" && password == "password") || (dbContext.users.SingleOrDefault(x => x.Name == userName && x.Password == HashSha256(password))) != null)

                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

        }

        public void DeleteUser(int id)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                var user = dbContext.users.Where(x => x.ID == id).FirstOrDefault();
                dbContext.users.Remove(user);
                dbContext.SaveChanges();
            }
        }
        public User GetUserById(int id)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                return dbContext.users.Where(x => x.ID == id).FirstOrDefault();
            }
        }

        public void EditExistingUser(User editedUser)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                var user = dbContext.users.Where(x => x.ID == editedUser.ID).FirstOrDefault();
                user.Name = editedUser.Name;
                user.Password = editedUser.Password;
                dbContext.users.Update(user);
                dbContext.SaveChanges();
            }
        }

        public string HashSha256(string randomString)
        {
            SHA256Managed crypt = new SHA256Managed();
            string hash = string.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash += theByte.ToString("x2");
            }
            return hash;
        }

    }

}

