﻿using System.Collections.Generic;
using System.Linq;
using VideoPresentation.Connection;
using VideoPresentation.Entities;
using VideoPresentation.Models;

namespace VideoPresentation.Infrastructure.Database
{
    public class DBAuthorsRepository : IAuthorsRepository
    {
        public DBAuthorsRepository()
        {
        }

        public void SaveAuthor(Author author)
        {

            using (VideoDbContext dbContext = new VideoDbContext())
            {
                dbContext.authors.Add(author);
                dbContext.SaveChanges();
            }

        }

        public List<Author> GetAllAuthors()
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                return dbContext.authors.ToList();
            }
        }
        public void DeleteAuthor(int id)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                var author = dbContext.authors.Where(x => x.AuthId == id).FirstOrDefault();
                dbContext.authors.Remove(author);
                dbContext.SaveChanges();
            }
        }

        public Author GetAuthorById(int id)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                return dbContext.authors.Where(x => x.AuthId == id).FirstOrDefault();
            }
        }

        public void EditExistingAuthor(Author editAuthor)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                var author = dbContext.authors.Where(x => x.AuthId == editAuthor.AuthId).FirstOrDefault();
                author.Name = editAuthor.Name;
                dbContext.authors.Update(author);
                dbContext.SaveChanges();
            }
        }

        public List<Author> SearchAuthor(string fileName)
        {
            using (VideoDbContext dbContext = new VideoDbContext())
            {
                return dbContext.authors.Where(x => x.Name.Contains(fileName)).ToList();
            }
        }

}
    }