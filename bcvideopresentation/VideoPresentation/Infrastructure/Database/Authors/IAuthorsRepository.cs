﻿using VideoPresentation.Models;
using System.Collections.Generic;
using VideoPresentation.Entities;

namespace VideoPresentation.Infrastructure.Database
{
    public interface IAuthorsRepository
    {
        void SaveAuthor(Author author);
        List<Author> GetAllAuthors();
        Author GetAuthorById(int id);
        void DeleteAuthor(int id);
        void EditExistingAuthor(Author editAuthor);
        public List<Author> SearchAuthor(string fileName);
    }
}
       
