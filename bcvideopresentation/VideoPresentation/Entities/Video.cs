﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VideoPresentation.Entities
{
    public class Video
    {
        public Video()
        {

        }
        public Video(string videoTitle, int coAuthId ,string videoName, int authId, string videoCommentCs, string videoCommentEn, string authorCommentCs, string authorCommentEn, int year, int additionalYear, string series, string prequelCs, string prequelEn, string citationCs, string citationEn, string artisticMedium, string technicalMedium, string performerCs, string performerEs, string toolName, string toolAuthor, string toolInfo, string annottationCs, string annottationEn, string annottationReferenceCs, string annottationReferenceEn, string sourcesCs, string sourcesEn, string muniSource, int videoLength, int additionalVideoLength,string additonalAuthor)
        {
            this.VideoTitle = videoTitle;

            this.VideoName = videoName;
            this.AuthId = authId;
            this.VideoCommentCs = videoCommentCs;
            this.VideoCommentEn = videoCommentEn;
            this.AuthorCommentCs = authorCommentCs;
            this.AuthorCommentEn = authorCommentEn;
            this.Year = year;
            this.AdditionalYear = additionalYear;
            this.Series = series;
            this.PrequelCs = prequelCs;
            this.PrequelEn = prequelEn;
            this.CitationCs = citationCs;
            this.CitationEn = citationEn;
            this.ArtisticMedium = artisticMedium;
            this.TechnicalMedium = technicalMedium;
            this.PerformerCs = performerCs;
            this.PerformerEn = performerEs;
            this.VoicePerformerCs = videoCommentCs;
            this.VoicePerformerEn = videoCommentEn;
            this.ToolName = toolName;
            this.ToolAuthor = toolAuthor;
            this.ToolInfo = toolInfo;
            this.AnnotationCs = annottationCs;
            this.AnnotationEn = annottationEn;
            this.AnnotationReferenceCs = annottationReferenceCs;
            this.AnnotationReferenceEn = annottationReferenceEn;
            this.SourcesCs = sourcesCs;
            this.SourcesEn = sourcesEn;
            this.MuniSource = muniSource;
            this.VideoLength = videoLength;
            this.AdditionalVideoLength = additionalVideoLength;
            this.CoAuthId = coAuthId;
            this.AdditonalAuthor = additonalAuthor;
        }

        

        public int ID { get; set; }
        public string VideoTitle { get; set; }
        public string VideoName { get; set; }
        public int AuthId { get; set; }
        public int CoAuthId { get; set; }
        public string VideoCommentCs { get; set; }
        public string VideoCommentEn { get; set; }
        public string AuthorCommentCs { get; set; }
        public string AuthorCommentEn { get; set; }
        public int Year { get; set; }
        public int AdditionalYear { get; set; }
        public string Series { get; set; }
        public string PrequelCs { get; set; }
        public string PrequelEn { get; set; }
        public string CitationCs { get; set; }
        public string CitationEn { get; set; }
        public string ArtisticMedium { get; set; }
        public string TechnicalMedium { get; set; }
        public string PerformerCs { get; set; }
        public string PerformerEn { get; set; }
        public string VoicePerformerCs { get; set; }
        public string VoicePerformerEn { get; set; }
        public string ToolName { get; set; }
        public string ToolAuthor { get; set; }
        public string ToolInfo { get; set; }
        public string AnnotationCs { get; set; }
        public string AnnotationEn { get; set; }
        public string AnnotationReferenceCs { get; set; }
        public string AnnotationReferenceEn { get; set; }
        public string SourcesCs { get; set; }
        public string SourcesEn { get; set; }
        public string MuniSource { get; set; }
        public int VideoLength { get; set; }
        public int AdditionalVideoLength { get; set; }
        public string AdditonalAuthor { get; set; }
    }
}
