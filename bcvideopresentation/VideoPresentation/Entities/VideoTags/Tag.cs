﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoPresentation.Infrastructure.Database.VideoTags
{
    public class Tag
    {
        public Tag()
        {

        }

        public Tag(int iD, string name)
        {
            ID = iD;
            Name = name;
        }

        public int ID { get; set; }
        public string Name { get; set; }
    }
}
