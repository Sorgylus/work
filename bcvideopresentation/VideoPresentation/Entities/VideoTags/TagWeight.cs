﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoPresentation.Infrastructure.Database.VideoTags
{
    public class TagWeight
    {
        public int ID { get; set; }
        public int Percentage { get; set; }
        public int VideoID { get; set; }

        public Tag Tag { get; set; }
    }
}
