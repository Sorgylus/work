﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoPresentation.Infrastructure.Database.VideoTags
{
    public class TagPosition
    {
        public int ID { get; set; }
        public float Time { get; set; }
        public float Prediction { get; set; }
        public int VideoID { get; set; }

        public Tag Tag { get; set; }
    }
}
