﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoPresentation.Entities.JSON
{
    public class TagsJson
    {
        public class TagsInfoData
        {
            public TagsInfoData(TagInfo[] tagInfos, string tagName)
            {
                TagInfos = tagInfos;
                TagName = tagName;
            }

            public TagInfo[] TagInfos { get; private set; }
            public string TagName { get; private set; }
        }

        public class Rootobject
        {

            public Rootobject()
            {
            }

            public TagInfo[] Body { get; set; }
            public TagInfo[] Digit { get; set; }
            public TagInfo[] Effect { get; set; }
            [JsonProperty("Element (air)")]
            public TagInfo[] Elementair { get; set; }
            [JsonProperty("Element (earth)")]
            public TagInfo[] Elementearth { get; set; }
            [JsonProperty("Element (fire)")]
            public TagInfo[] Elementfire { get; set; }
            [JsonProperty("Element (water)")]
            public TagInfo[] Elementwater { get; set; }
            public TagInfo[] Face { get; set; }
            [JsonProperty("Image processing keying")]
            public TagInfo[] Imageprocessingkeying { get; set; }
            public TagInfo[] Interior { get; set; }
            public TagInfo[] Landscape { get; set; }
            public TagInfo[] Letter { get; set; }
            [JsonProperty("Machine (TV set)")]
            public TagInfo[] MachineTVset { get; set; }
            [JsonProperty("Machine (car)")]
            public TagInfo[] Machinecar { get; set; }
            [JsonProperty("Machine vision (fisheye)")]
            public TagInfo[] Machinevisionfisheye { get; set; }
            [JsonProperty("RuttEtra video synthesizer")]
            public TagInfo[] RuttEtravideosynthesizer { get; set; }
            public TagInfo[] Steina { get; set; }
            public TagInfo[] Violin { get; set; }
            public TagInfo[] Woody { get; set; }
            public TagInfo[] Stripes { get; set; }

            public List<TagsInfoData> GetTagListsList()
            {
                return new List<TagsInfoData>()
                {
                   new TagsInfoData(Body, nameof(Body)) ,
            new TagsInfoData( Digit, nameof(Digit)),
            new TagsInfoData( Effect , nameof(Effect)),
            new TagsInfoData( Elementair , nameof(Elementair)),
            new TagsInfoData( Elementearth , nameof(Elementearth)),
            new TagsInfoData( Elementfire , nameof(Elementfire)),
            new TagsInfoData( Elementwater , nameof(Elementwater)),
            new TagsInfoData( Face , nameof(Face)),
            new TagsInfoData( Imageprocessingkeying , nameof(Imageprocessingkeying)),
            new TagsInfoData( Interior , nameof(Interior)),
            new TagsInfoData( Landscape , nameof(Landscape)),
            new TagsInfoData( Letter , nameof(Letter)),
            new TagsInfoData( MachineTVset , nameof(MachineTVset)),
            new TagsInfoData( Machinecar , nameof(Machinecar)),
            new TagsInfoData( Machinevisionfisheye , nameof(Machinevisionfisheye)),
            new TagsInfoData( RuttEtravideosynthesizer , nameof(RuttEtravideosynthesizer)),
            new TagsInfoData( Steina , nameof(Steina)),
            new TagsInfoData( Violin , nameof(Violin)),
            new TagsInfoData( Woody , nameof(Woody)),
            new TagsInfoData( Stripes, nameof(Stripes)),
        };
            }
        }

        public class TagInfo
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }



    }
}
