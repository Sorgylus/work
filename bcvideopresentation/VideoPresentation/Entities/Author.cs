﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VideoPresentation.Entities
{
    public class Author
    {
        public Author()
        {

        }
        public Author(string name)
        {
            this.Name = name;            
            
        }
        [Key]
        public int AuthId { get; set; }

        public string Name { get; set; }


    }
}
