﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VideoPresentation.Models;
using MySql.Data.MySqlClient;
using MySql.Data.EntityFrameworkCore.Extensions;
using VideoPresentation.Entities;
using VideoPresentation.Infrastructure.Database.VideoTags;

namespace VideoPresentation.Connection
{
    public class VideoDbContext : DbContext
    {
        public VideoDbContext()
        {

        }
        public DbSet<Video> videos { get; set; }
        public DbSet<User> users { get; set; }
        public DbSet<Author> authors { get; set; }
        public DbSet<Tag> tags { get; set; }
        public DbSet<TagPosition> tagPositions { get; set; }
        public DbSet<TagWeight> TagWeight { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server = localhost; user id = root; database = video_presentation; password = 123456789;");
            base.OnConfiguring(optionsBuilder);

        }

    
    }
}
