#pragma checksum "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9742d3a5eee52e3c72b83c3dedc959e91932b1ee"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Video_TagsResults), @"mvc.1.0.view", @"/Views/Video/TagsResults.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\_ViewImports.cshtml"
using VideoPresentation;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\_ViewImports.cshtml"
using VideoPresentation.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9742d3a5eee52e3c72b83c3dedc959e91932b1ee", @"/Views/Video/TagsResults.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"bf0e477d604ceb1a1b7d99b336d53aeb9e6e5340", @"/Views/_ViewImports.cshtml")]
    public class Views_Video_TagsResults : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<UploadModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
            WriteLiteral("<!DOCTYPE html>\r\n");
#nullable restore
#line 4 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
  
    ViewData["Title"] = "Video";
    Layout = "~/Views/Shared/Video.cshtml";

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
  int i = 0;

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 11 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
 foreach (UploadModel v in Model)
{


    if (i == 0)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"tags-buttons\">\r\n");
#nullable restore
#line 18 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
             foreach (VideoPresentation.Infrastructure.Database.VideoTags.TagWeight tag in v.TagWeights)
            {


#line default
#line hidden
#nullable disable
            WriteLiteral("                <div>\r\n                    <button class=\"tag-button tag-button-action\"><a");
            BeginWriteAttribute("href", " href=\"", 535, "\"", 579, 2);
            WriteAttributeValue("", 542, "/Video/TagsResults/?tagID=", 542, 26, true);
#nullable restore
#line 22 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
WriteAttributeValue("", 568, tag.Tag.ID, 568, 11, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 22 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
                                                                                                            Write(tag.Tag.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a></button>\r\n                </div>\r\n");
#nullable restore
#line 24 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"

            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </div>\r\n");
#nullable restore
#line 27 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
        i++;
    }


#line default
#line hidden
#nullable disable
            WriteLiteral(@"    <script>
        var vid = document.getElementById(""loadedVideo"");
        vid.onloadedmetadata = function myFunction() {
            var x, i;
            x = document.querySelectorAll("".video-time"");
            for (i = 0; i < x.length; i++) {
                x[i].currentTime = 10;
            }
        }
    </script>
");
            WriteLiteral("    <div class=\"resultsbox\">\r\n        <div class=\"results\">\r\n            <a");
            BeginWriteAttribute("href", " href=\"", 1106, "\"", 1133, 2);
            WriteAttributeValue("", 1113, "/Video/Play?id=", 1113, 15, true);
#nullable restore
#line 44 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
WriteAttributeValue("", 1128, v.ID, 1128, 5, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                <div class=\"video-square\">\r\n                    <video class=\"video-time\" id=\"loadedVideo\">\r\n                        <source");
            BeginWriteAttribute("src", " src=\"", 1277, "\"", 1322, 1);
#nullable restore
#line 47 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
WriteAttributeValue("", 1283, Url.Content("~/upload/" + v.VideoName), 1283, 39, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n                    </video>\r\n                </div>\r\n            </a>\r\n\r\n            <div style=\"text-align:center; margin-top:25px\">\r\n                ");
#nullable restore
#line 53 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
           Write(v.VideoTitle);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 54 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
                 if (v.Year != -1)
                {
                    

#line default
#line hidden
#nullable disable
#nullable restore
#line 56 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
               Write(v.Year);

#line default
#line hidden
#nullable disable
#nullable restore
#line 56 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
                           
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </div>\r\n            <br />\r\n            <br />\r\n            Tagy\r\n");
#nullable restore
#line 62 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
             foreach (VideoPresentation.Infrastructure.Database.VideoTags.TagWeight tag in v.TagWeights)
            {
                

#line default
#line hidden
#nullable disable
#nullable restore
#line 64 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
                 if (tag.Percentage != 0)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <div tyle=\"text-align:left\">");
#nullable restore
#line 66 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
                                           Write(tag.Tag.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral(": ");
#nullable restore
#line 66 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
                                                          Write(tag.Percentage);

#line default
#line hidden
#nullable disable
            WriteLiteral("%</div>\r\n");
#nullable restore
#line 67 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
                }

#line default
#line hidden
#nullable disable
#nullable restore
#line 67 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
                 

            }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 71 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
             if (User.IsInRole("Admin"))
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <a");
            BeginWriteAttribute("href", " href=\"", 2059, "\"", 2094, 2);
            WriteAttributeValue("", 2066, "/Video/DeleteVideo/?id=", 2066, 23, true);
#nullable restore
#line 73 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
WriteAttributeValue("", 2089, v.ID, 2089, 5, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"button-action button-action-delete\">");
#nullable restore
#line 73 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
                                                                                             Write(localizer["Delete"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                <a");
            BeginWriteAttribute("href", " href=\"", 2183, "\"", 2211, 2);
            WriteAttributeValue("", 2190, "/Video/Edit/?id=", 2190, 16, true);
#nullable restore
#line 74 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
WriteAttributeValue("", 2206, v.ID, 2206, 5, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"button-action button-action-edit\">");
#nullable restore
#line 74 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
                                                                                    Write(localizer["Edit"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n");
#nullable restore
#line 75 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("            <br />\r\n            <br />\r\n\r\n        </div>\r\n    </div>\r\n");
#nullable restore
#line 81 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\TagsResults.cshtml"






}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Microsoft.AspNetCore.Mvc.Localization.IViewLocalizer localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<UploadModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
