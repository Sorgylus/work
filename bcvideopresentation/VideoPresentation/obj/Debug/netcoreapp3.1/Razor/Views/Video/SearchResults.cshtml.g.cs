#pragma checksum "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d81a383a92366adb292973f68f042915a001a645"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Video_SearchResults), @"mvc.1.0.view", @"/Views/Video/SearchResults.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\_ViewImports.cshtml"
using VideoPresentation;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\_ViewImports.cshtml"
using VideoPresentation.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d81a383a92366adb292973f68f042915a001a645", @"/Views/Video/SearchResults.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"bf0e477d604ceb1a1b7d99b336d53aeb9e6e5340", @"/Views/_ViewImports.cshtml")]
    public class Views_Video_SearchResults : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<VideoPresentation.Entities.Video>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("text-align:center; margin-top:25px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<!DOCTYPE html>\r\n");
#nullable restore
#line 3 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
  
    ViewData["Title"] = "Video";
    Layout = "~/Views/Shared/Video.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n");
#nullable restore
#line 10 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
 if (Model.Count() == 0)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <tr>\r\n        <H1 style=\"color: #ff7000;text-align: center\">\r\n            ");
#nullable restore
#line 14 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
       Write(localizer["NoMatch"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </H1>\r\n    </tr>\r\n");
#nullable restore
#line 17 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 19 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
 foreach (VideoPresentation.Entities.Video v in Model)
{


#line default
#line hidden
#nullable disable
            WriteLiteral(@"    <script>
        var vid = document.getElementById(""loadedVideo"");
        vid.onloadedmetadata = function myFunction() {
            var x, i;
            x = document.querySelectorAll("".video-time"");
            for (i = 0; i < x.length; i++) {
                x[i].currentTime = 10;
            }
        }
    </script>
    <div class=""resultsbox"">
        <div class=""results"">
            <div class=""video-square"">
                <video class=""video-time"" id=""loadedVideo"">
                    <source");
            BeginWriteAttribute("src", " src=\"", 975, "\"", 1020, 1);
#nullable restore
#line 36 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
WriteAttributeValue("", 981, Url.Content("~/upload/" + v.VideoName), 981, 39, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" />\r\n                </video>\r\n            </div>\r\n\r\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d81a383a92366adb292973f68f042915a001a6456403", async() => {
                WriteLiteral("\r\n                ");
#nullable restore
#line 41 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
           Write(v.VideoTitle);

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n");
#nullable restore
#line 42 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
                 if (v.Year != -1)
                {
                    

#line default
#line hidden
#nullable disable
#nullable restore
#line 44 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
               Write(v.Year);

#line default
#line hidden
#nullable disable
#nullable restore
#line 44 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
                           
                }

#line default
#line hidden
#nullable disable
                WriteLiteral("                <br />\r\n\r\n");
#nullable restore
#line 48 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
                 if (User.IsInRole("Admin"))
                {

#line default
#line hidden
#nullable disable
                WriteLiteral("                    <a");
                BeginWriteAttribute("href", " href=\"", 1384, "\"", 1419, 2);
                WriteAttributeValue("", 1391, "/Video/DeleteVideo/?id=", 1391, 23, true);
#nullable restore
#line 50 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
WriteAttributeValue("", 1414, v.ID, 1414, 5, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"button-action button-action-delete\">");
#nullable restore
#line 50 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
                                                                                                 Write(localizer["Delete"]);

#line default
#line hidden
#nullable disable
                WriteLiteral("</a>\r\n                    <a");
                BeginWriteAttribute("href", " href=\"", 1512, "\"", 1540, 2);
                WriteAttributeValue("", 1519, "/Video/Edit/?id=", 1519, 16, true);
#nullable restore
#line 51 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
WriteAttributeValue("", 1535, v.ID, 1535, 5, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"button-action button-action-edit\">");
#nullable restore
#line 51 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
                                                                                        Write(localizer["Edit"]);

#line default
#line hidden
#nullable disable
                WriteLiteral("</a>\r\n");
#nullable restore
#line 52 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"
                }

#line default
#line hidden
#nullable disable
                WriteLiteral("            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n        </div>\r\n    </div>\r\n");
#nullable restore
#line 56 "D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\Views\Video\SearchResults.cshtml"

}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Microsoft.AspNetCore.Mvc.Localization.IViewLocalizer localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<VideoPresentation.Entities.Video>> Html { get; private set; }
    }
}
#pragma warning restore 1591
