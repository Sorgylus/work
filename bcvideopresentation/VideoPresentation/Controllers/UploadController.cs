﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VideoPresentation.Models;
using MySql.Data.MySqlClient;
using System.Data;
using VideoPresentation.Connection;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using VideoPresentation.Entities;
using VideoPresentation.Entities.JSON;
using VideoPresentation.Infrastructure.Database.VideoTags;

namespace VideoPresentation.Controllers
{

    public class UploadController : Controller
    {
        private readonly ILogger<UploadController> _logger;
        private Infrastructure.Database.IVideosRepository videosRepository;
        private Infrastructure.Database.IAuthorsRepository authorRepository;
        public UploadController(ILogger<UploadController> logger, Infrastructure.Database.IVideosRepository videosRepository, Infrastructure.Database.IAuthorsRepository authorRepository)
        {
            _logger = logger;

            this.videosRepository = videosRepository ?? throw new ArgumentNullException(nameof(videosRepository));
            this.authorRepository = authorRepository;
        }


        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public IActionResult Upload()
        {
            UploadModel um = new UploadModel();
            um.Authors = authorRepository.GetAllAuthors();
            return View(um);
        }

        [HttpPost]
        public IActionResult UploadFile(UploadModel addVideo, List<IFormFile> uploadFile, List<IFormFile> uploadJson)
        {

            int isNull = -1;

            MemoryStream ms = new MemoryStream();
            uploadFile[0].CopyTo(ms);
            Video videoToAdd = addVideo.CreateVideo();


            if (videoToAdd.Year == 0)
            {
                videoToAdd.Year = isNull;
            }

            if (videoToAdd.AdditionalYear == 0)
            {
                videoToAdd.AdditionalYear = isNull;
            }


            if (videoToAdd.VideoLength == 0)
            {
                videoToAdd.VideoLength = isNull;
            }


            if (videoToAdd.AdditionalVideoLength == 0)
            {
                videoToAdd.AdditionalVideoLength = isNull;
            }

            byte[] fileBytes = ms.ToArray();

            System.IO.File.WriteAllBytes(@"D:\Matúš\VUT škola\3 ročník\5 semester\Bakalárka\program\bcvideopresentation\VideoPresentation\wwwroot\upload\" + uploadFile[0].FileName, fileBytes);

            ms.Dispose();
            ms = new MemoryStream();

            uploadJson[0].CopyTo(ms);

            byte[] jsonBytes = ms.ToArray();



            string json = System.Text.Encoding.UTF8.GetString(jsonBytes);
          
            videosRepository.SaveVideo(videoToAdd, ms.ToArray(), uploadFile[0].FileName, json);



            return RedirectToAction("Upload");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }

}
