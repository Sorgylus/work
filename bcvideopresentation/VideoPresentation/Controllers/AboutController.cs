﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VideoPresentation.Models;

namespace VideoPresentation.Controllers
{
    public class AboutController : Controller
    {

        public IActionResult Dataset()
        {
            return View();
        }
        public IActionResult Findings()
        {
            return View();
        }
        public IActionResult Introduction()
        {
            return View();
        }
        public IActionResult Live_archive()
        {
            return View();
        }
        public IActionResult Results()
        {
            return View();
        }
        public IActionResult Theory()
        {
            return View();
        }
        public IActionResult IntelligentAtlas()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

}
