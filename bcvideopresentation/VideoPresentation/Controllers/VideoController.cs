﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VideoPresentation.Models;
using MySql.Data.MySqlClient;
using System.Data;
using VideoPresentation.Connection;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using VideoPresentation.Entities;
using VideoPresentation.Infrastructure.Database.VideoTags;
using System.Web.Helpers;

namespace VideoPresentation.Controllers
{

    public class VideoController : Controller
    {
        private Infrastructure.Database.IVideosRepository videosRepository;
        private Infrastructure.Database.IAuthorsRepository authorsRepositroy;
        public VideoController(Infrastructure.Database.IVideosRepository videoRepository, Infrastructure.Database.IAuthorsRepository authorRepositroy)
        {
            this.videosRepository = videoRepository;
            this.authorsRepositroy = authorRepositroy;       
        }

        [HttpPost]
        public IActionResult DeleteVideo(Video video)
        {
            videosRepository.RemoveVideo(video);

            return RedirectToAction("DbView");
        }
        public IActionResult DeleteVideo(int id)
        {
            Video v = videosRepository.GetVideoById(id);
            return View(v);
        }
        public IActionResult SearchResults(string fileName)
        {
            List<Video> videolist = videosRepository.SearchVideo(fileName);
            return View(videolist);
        }
        public IActionResult TagsResults(int tagID)
        {
            List<Video> videolist = videosRepository.GetVideosWithTag(tagID);
            List<UploadModel> umList = new List<UploadModel>();
            List<Author> authList = authorsRepositroy.GetAllAuthors();
            foreach (Video v in videolist)
            {
                UploadModel um = new UploadModel(v, authList);
                um.TagWeights = videosRepository.GetTagWeightsForVideo(v.ID);
                umList.Add(um);
            }

            return View(umList);
        }

        public IActionResult DbView()
        {
            List<Video> videolist = videosRepository.GetAllVideos();
            List<UploadModel> umList = new List<UploadModel>();
            List<Author> authList = authorsRepositroy.GetAllAuthors();
            foreach (Video v in videolist)
            {
                UploadModel um = new UploadModel(v, authList);
                um.TagWeights = videosRepository.GetTagWeightsForVideo(v.ID);
                umList.Add(um);
            }

            return View(umList);
        }
        public IActionResult GetJsonTags(int id)
        {
            List<TagPosition> taglist = videosRepository.GetJsonTagById(id);
            return new JsonResult(taglist);
        }


        public IActionResult Play(int id)
        {
            Video v = videosRepository.GetVideoById(id);
            List<Author> authorsList = authorsRepositroy.GetAllAuthors();
            Author autor = authorsRepositroy.GetAuthorById(v.AuthId);
            UploadModel um = new UploadModel(v, autor);

            UploadModel um2 = new UploadModel(v, authorsRepositroy.GetAllAuthors());

            return View(um2);
        }
        [Authorize(Roles = "Admin, User")]
        public IActionResult Edit(int id)
        {
            new UploadModel();
            Video v = videosRepository.GetVideoById(id);
            Author author = authorsRepositroy.GetAuthorById(v.AuthId);
            UploadModel um = new UploadModel(v, authorsRepositroy.GetAllAuthors());
            return View(um);
        }
       
        [Authorize(Roles = "Admin, User")]
        [HttpPost]
        public IActionResult Edit(UploadModel videoModel)
        {           
            videosRepository.EditVideo(videoModel.CreateVideo());
            return Redirect("DbView");
        }
    }

}
