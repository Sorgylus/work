﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using VideoPresentation.Models;

namespace VideoPresentation.Controllers
{
    public class MachineVisionController : Controller
    {
        public IActionResult MachineVision()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
