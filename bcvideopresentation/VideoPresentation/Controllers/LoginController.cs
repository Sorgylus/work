﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VideoPresentation.Models;
using MySql.Data.MySqlClient;
using System.Data;
using VideoPresentation.Connection;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using VideoPresentation.Entities;
using Microsoft.Extensions.Localization;

namespace VideoPresentation.Controllers

{
    public class LoginController : Controller
    {
        private readonly IStringLocalizer<LoginController> _localizer;
        private ILogger<LoginController> _logger;
        private Infrastructure.Database.IUsersRepository usersRepository;
        private Infrastructure.Database.IAuthorsRepository authorsRepository;

        public LoginController(ILogger<LoginController> logger, Infrastructure.Database.IUsersRepository usersRepository, Infrastructure.Database.IAuthorsRepository authorsRepository, IStringLocalizer<LoginController> localizer)
        {
            _logger = logger;
            _localizer = localizer;

            this.usersRepository = usersRepository ?? throw new ArgumentNullException(nameof(usersRepository));
            this.authorsRepository = authorsRepository ?? throw new ArgumentNullException(nameof(authorsRepository));
        }


        public IActionResult Login()
        {
            return View();
        }



        [HttpPost]
        public IActionResult login(string userName, string password)
        {

            if (usersRepository.UserCheck(userName, password))
            {
                System.Security.Principal.IIdentity identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, userName),
                    new Claim(ClaimTypes.Role, "Admin")
                }, CookieAuthenticationDefaults.AuthenticationScheme);

                ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                return RedirectToAction("Login");
            }
            else
            {
                TempData["LoginMessage"] = "password or username is incorect";
                return RedirectToAction("Login");
            }
        }


        public IActionResult Logout()
        {
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
        }
        [Authorize(Roles = "Admin, User")]
        public IActionResult AddUser()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddNewUser(User user)
        {
            usersRepository.SaveUser(user);
            return Redirect("AddUser");
        }
        [Authorize(Roles = "Admin, User")]
        public IActionResult AllUsers()
        {
            List<User> userlist = usersRepository.GetAllUsers();
            return View(userlist);
        }
        [Authorize(Roles = "Admin, User")]
        public IActionResult AddAuthor()
        {
            return View();
        }
        [Authorize(Roles = "Admin, User")]
        public IActionResult RemoveUser(int id)
        {
            usersRepository.DeleteUser(id);

            return RedirectToAction("AllUsers");
        }
        [Authorize(Roles = "Admin, User")]
        public IActionResult EditUser(int id)
        {
            User u = usersRepository.GetUserById(id);
            return View(u);
        }

        [HttpPost]
        public IActionResult EditUser(User user)
        {
            usersRepository.EditExistingUser(user);
            return Redirect("AllUsers");
        }

        [HttpPost]
        public IActionResult AddNewAuthor(Author author)
        {
            authorsRepository.SaveAuthor(author);
            return Redirect("AddAuthor");
        }
        [Authorize(Roles = "Admin, User")]
        public IActionResult AllAuthors()
        {
            List<Author> authorlist = authorsRepository.GetAllAuthors();
            return View(authorlist);
        }
        [Authorize(Roles = "Admin, User")]

        public IActionResult RemoveAuthor(int id)
        {
            authorsRepository.DeleteAuthor(id);

            return RedirectToAction("AllAuthors");
        }
        [Authorize(Roles = "Admin, User")]
        public IActionResult EditAuthor(int id)
        {
            Author a = authorsRepository.GetAuthorById(id);
            return View(a);
        }

        [HttpPost]
        public IActionResult EditAuthor(Author author)
        {
            authorsRepository.EditExistingAuthor(author);
            return Redirect("AllAuthors");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
