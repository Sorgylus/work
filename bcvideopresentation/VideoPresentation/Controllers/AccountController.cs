﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoPresentation.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult login()
        {
            return Redirect("~/Login/login");
        }
    }
}
