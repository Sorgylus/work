﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VideoPresentation.Models
{
    public class JsonModel
    {

        public class Rootobject
        {
            public Body[] Body { get; set; }
            public Digit[] Digit { get; set; }
            public Effect[] Effect { get; set; }
            public ElementAir[] Elementair { get; set; }
            public ElementEarth[] Elementearth { get; set; }
            public ElementFire[] Elementfire { get; set; }
            public ElementWater[] Elementwater { get; set; }
            public Face[] Face { get; set; }
            public ImageProcessingKeying[] Imageprocessingkeying { get; set; }
            public Interior[] Interior { get; set; }
            public Landscape[] Landscape { get; set; }
            public Letter[] Letter { get; set; }
            public MachineTVSet[] MachineTVset { get; set; }
            public MachineCar[] Machinecar { get; set; }
            public MachineVisionFisheye[] Machinevisionfisheye { get; set; }
            public RuttetraVideoSynthesizer[] RuttEtravideosynthesizer { get; set; }
            public Steina[] Steina { get; set; }
            public Violin[] Violin { get; set; }
            public Woody[] Woody { get; set; }
            public Stripe[] Stripes { get; set; }
        }

        public class Body
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class Digit
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class Effect
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class ElementAir
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class ElementEarth
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class ElementFire
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class ElementWater
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class Face
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class ImageProcessingKeying
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class Interior
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class Landscape
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class Letter
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class MachineTVSet
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class MachineCar
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class MachineVisionFisheye
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class RuttetraVideoSynthesizer
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class Steina
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class Violin
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class Woody
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

        public class Stripe
        {
            public float time { get; set; }
            public float prediction { get; set; }
        }

    }
}
