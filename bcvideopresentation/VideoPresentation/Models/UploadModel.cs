﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VideoPresentation.Entities;
using VideoPresentation.Infrastructure.Database.VideoTags;

namespace VideoPresentation.Models
{
    public class UploadModel
    {


        public UploadModel()
        {

        }

        public UploadModel(Video v, Author a)
        {
            this.VideoTitle = v.VideoTitle;
            this.VideoCommentCs = v.VideoCommentCs;
            this.VideoCommentEn = v.VideoCommentEn;
            this.AuthorCommentCs = v.AuthorCommentCs;
            this.AuthorCommentEn = v.AuthorCommentEn;
            this.Year = v.Year;
            this.AdditionalYear = v.AdditionalYear;
            this.Series = v.Series;
            this.PrequelCs = v.PrequelCs;
            this.PrequelEn = v.PrequelEn;
            this.CitationCs = v.CitationCs;
            this.CitationEn = v.CitationEn;
            this.ArtisticMedium = v.ArtisticMedium;
            this.TechnicalMedium = v.TechnicalMedium;
            this.PerformerCs = v.PerformerCs;
            this.PerformerEn = v.PerformerEn;
            this.VoicePerformerCs = v.VoicePerformerCs;
            this.VoicePerformerEn = v.VoicePerformerEn;
            this.ToolName = v.ToolName;
            this.ToolAuthor = v.ToolAuthor;
            this.ToolInfo = v.ToolInfo;
            this.AnnotationCs = v.AnnotationCs;
            this.AnnotationEn = v.AnnotationEn;
            this.AnnotationReferenceCs = v.AnnotationReferenceCs;
            this.AnnotationReferenceEn = v.AnnotationReferenceEn;
            this.SourcesCs = v.SourcesCs;
            this.SourcesEn = v.SourcesEn;
            this.MuniSource = v.MuniSource;
            this.VideoLength = v.VideoLength;
            this.AdditionalVideoLength = v.AdditionalVideoLength;
            this.SelectedAuthor = v.AuthId;
            this.VideoName = v.VideoName;
            this.SelectedCoAuthor = v.CoAuthId;
            this.AutorName = a.Name;
            this.CoAutorName = a.Name;
        }

        public UploadModel(Video v, IList<Author> authors)
        {
            this.VideoTitle = v.VideoTitle;
            this.VideoCommentCs = v.VideoCommentCs;
            this.VideoCommentEn = v.VideoCommentEn;
            this.AuthorCommentCs = v.AuthorCommentCs;
            this.AuthorCommentEn = v.AuthorCommentEn;
            this.Year = v.Year;
            this.AdditionalYear = v.AdditionalYear;
            this.Series = v.Series;
            this.PrequelCs = v.PrequelCs;
            this.PrequelEn = v.PrequelEn;
            this.CitationCs = v.CitationCs;
            this.CitationEn = v.CitationEn;
            this.ArtisticMedium = v.ArtisticMedium;
            this.TechnicalMedium = v.TechnicalMedium;
            this.PerformerCs = v.PerformerCs;
            this.PerformerEn = v.PerformerEn;
            this.VoicePerformerCs = v.VoicePerformerCs;
            this.VoicePerformerEn = v.VoicePerformerEn;
            this.ToolName = v.ToolName;
            this.ToolAuthor = v.ToolAuthor;
            this.ToolInfo = v.ToolInfo;
            this.AnnotationCs = v.AnnotationCs;
            this.AnnotationEn = v.AnnotationEn;
            this.AnnotationReferenceCs = v.AnnotationReferenceCs;
            this.AnnotationReferenceEn = v.AnnotationReferenceEn;
            this.SourcesCs = v.SourcesCs;
            this.SourcesEn = v.SourcesEn;
            this.MuniSource = v.MuniSource;
            this.VideoLength = v.VideoLength;
            this.AdditionalVideoLength = v.AdditionalVideoLength;
            this.SelectedAuthor = v.AuthId;
            this.VideoName = v.VideoName;
            this.SelectedCoAuthor = v.CoAuthId;
            this.ID = v.ID;
            this.Authors = authors;


            Author foundedAuthor = null;
            for (int i = 0; i < authors.Count; i++)
                if (authors[i].AuthId == v.AuthId)
                {
                    foundedAuthor = authors[i];
                    break;
                }
            Author foundedCoAuthor = null;
            for (int i = 0; i < authors.Count; i++)
                if (authors[i].AuthId == v.CoAuthId)
                {
                    foundedCoAuthor = authors[i];
                    break;
                }

            this.AutorName = foundedAuthor.Name;
            this.CoAutorName = foundedCoAuthor.Name;
        }

        public UploadModel(IList<Video> videolist, IList<Author> authorlist)
        {
            this.Videos = videolist;
            this.Authors = authorlist;
        }

        public IList<Author> Authors { get; set; }
        public IList<Video> Videos { get; set; }
        public IList<TagWeight> TagWeights { get; set; }
        public int SelectedAuthor { get; set; }
        public int SelectedCoAuthor { get; set; }
        public int ID { get; set; }
        public string VideoTitle { get; set; }
        public string VideoName { get; set; }
        public int AuthId { get; set; }
        public int CoAuthId { get; set; }
        public string AdditionalAuthor { get; set; }
        public string VideoCommentCs { get; set; }
        public string VideoCommentEn { get; set; }
        public string AuthorCommentCs { get; set; }
        public string AuthorCommentEn { get; set; }
        public int Year { get; set; }
        public int AdditionalYear { get; set; }
        public string Series { get; set; }
        public string PrequelCs { get; set; }
        public string PrequelEn { get; set; }
        public string CitationCs { get; set; }
        public string CitationEn { get; set; }
        public string ArtisticMedium { get; set; }
        public string TechnicalMedium { get; set; }
        public string PerformerCs { get; set; }
        public string PerformerEn { get; set; }
        public string VoicePerformerCs { get; set; }
        public string VoicePerformerEn { get; set; }
        public string ToolName { get; set; }
        public string ToolAuthor { get; set; }
        public string ToolInfo { get; set; }
        public string AnnotationCs { get; set; }
        public string AnnotationEn { get; set; }
        public string AnnotationReferenceCs { get; set; }
        public string AnnotationReferenceEn { get; set; }
        public string SourcesCs { get; set; }
        public string SourcesEn { get; set; }
        public string MuniSource { get; set; }
        public int VideoLength { get; set; }
        public int AdditionalVideoLength { get; set; }
        public string AutorName { get; set; }
        public string CoAutorName { get; set; }

        public Video CreateVideo()
        {
            Video rtnItem = new Video();
            rtnItem.ID = this.ID;
            rtnItem.VideoTitle = this.VideoTitle;
            rtnItem.VideoCommentCs = this.VideoCommentCs;
            rtnItem.VideoCommentEn = this.VideoCommentEn;
            rtnItem.AuthorCommentCs = this.AuthorCommentCs;
            rtnItem.AuthorCommentEn = this.AuthorCommentEn;
            rtnItem.Year = this.Year;
            rtnItem.AdditionalYear = this.AdditionalYear;
            rtnItem.Series = this.Series;
            rtnItem.PrequelCs = this.PrequelCs;
            rtnItem.PrequelEn = this.PrequelEn;
            rtnItem.CitationCs = this.CitationCs;
            rtnItem.CitationEn = this.CitationEn;
            rtnItem.ArtisticMedium = this.ArtisticMedium;
            rtnItem.TechnicalMedium = this.TechnicalMedium;
            rtnItem.PerformerCs = this.PerformerCs;
            rtnItem.PerformerEn = this.PerformerEn;
            rtnItem.VoicePerformerCs = this.VoicePerformerCs;
            rtnItem.VoicePerformerEn = this.VoicePerformerEn;
            rtnItem.ToolName = this.ToolName;
            rtnItem.ToolAuthor = this.ToolAuthor;
            rtnItem.ToolInfo = this.ToolInfo;
            rtnItem.AnnotationCs = this.AnnotationCs;
            rtnItem.AnnotationEn = this.AnnotationEn;
            rtnItem.AnnotationReferenceCs = this.AnnotationReferenceCs;
            rtnItem.AnnotationReferenceEn = this.AnnotationReferenceEn;
            rtnItem.SourcesCs = this.SourcesCs;
            rtnItem.SourcesEn = this.SourcesEn;
            rtnItem.MuniSource = this.MuniSource;
            rtnItem.VideoLength = this.VideoLength;
            rtnItem.AdditionalVideoLength = this.AdditionalVideoLength;
            rtnItem.AuthId = this.SelectedAuthor;
            rtnItem.CoAuthId = this.SelectedCoAuthor;
            rtnItem.AdditonalAuthor = this.AdditionalAuthor;
            return rtnItem;
        }
    }
}

